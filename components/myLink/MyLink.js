import React from "react";
import { useRouter } from "next/router";
import Link from "next/link";

export default function MyLink({
  children,
  className = "",
  activeClassName = "",
  href = "",
}) {
  const router = useRouter();
  return (
    <Link href={href}>
      <a
        className={
          router.pathname === href
            ? `${className} ${activeClassName}`
            : `${className}`
        }
      >
        {children}
      </a>
    </Link>
  );
}

/*
import { useRouter } from "next/router";
import { Link } from "next/link";

export default function MyLink({ children, className, activeClassName, href }) {
  const router = useRouter();

  return (
    <Link href={href}>
      <a
        className={
          router.pathname === href
            ? `${className} ${activeClassName}`
            : `${className}`
        }
      >
        {children}
      </a>
    </Link>
  );
} */

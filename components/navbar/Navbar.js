import Link from "next/link";
import styles from "../navbar/Navbar.module.scss";
import { useRouter } from "next/router";
import MyLink from "../myLink/MyLink";

export default function Navbar() {
  const router = useRouter();

  return (
    <nav className={styles.navbar}>
      <Link href="/">
        <a className={styles["navbar-brand"]}>Home</a>
      </Link>
      <div className={styles["navbar-link-container"]}>
        <MyLink
          href="/about"
          className={styles["navbar-link"]}
          activeClassName={styles["navbar-link-active"]}
        >
          About
        </MyLink>
        <MyLink
          href="/contact"
          className={styles["navbar-link"]}
          activeClassName={styles["navbar-link-active"]}
        >
          Contact
        </MyLink>
      </div>
    </nav>
  );
}

// import App from 'next/app'

import Navbar from "../components/navbar/Navbar";
import styles from "../sass/styles.scss";
import Head from "next/head";
function MyApp({ Component, pageProps }) {
  return (
    <div>
      <Head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="theme-color" content="#000000" />
        <meta
          name="description"
          content="Tailored Workforce is a website that does ..."
        />
        <link rel="icon" href="/static/favicon.ico" />
        <link rel="apple-touch-icon" href="/static/logo192.png" />
        <link rel="manifest" href="/static/manifest.json" />
        <title>Tailored Workforce</title>
      </Head>
      <Navbar />
      <Component {...pageProps} />
    </div>
  );
}

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.
//
// MyApp.getInitialProps = async (appContext) => {
//   // calls page's `getInitialProps` and fills `appProps.pageProps`
//   const appProps = await App.getInitialProps(appContext);
//
//   return { ...appProps }
// }

export default MyApp;
